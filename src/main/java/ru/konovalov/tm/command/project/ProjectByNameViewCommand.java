package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByNameViewCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-view-by-name";
    }

    @Override
    public @NotNull String description() {
        return "View project by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        showProject(serviceLocator.getProjectService().findOneByName(userId, TerminalUtil.nextLine()));
    }

}
